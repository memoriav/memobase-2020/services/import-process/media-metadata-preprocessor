/*
 * media-metadata-preprocessor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models.{
  BlankNode,
  BlankNodes,
  Literal,
  NtriplesStatement,
  URI
}
import org.scalatest.funsuite.AnyFunSuite
import scala.language.reflectiveCalls

class NtriplesObjectTest extends AnyFunSuite {

  val subject = "http://www.w3.org/2001/sw/RDFCore/ntriples/"
  val predicate = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
  val objUriLiteral = "http://xmlns.com/foaf/0.1/Document"
  val objLiteral = "astring"
  val indicator = "en-US"

  test(
    "A single N-Triples statement with URI as object should have valid syntax"
  ) {
    val obj = URI(objUriLiteral)
    val st = NtriplesStatement(subject, predicate, obj).toString
    assert(
      st == s"<$subject> <$predicate> <$objUriLiteral> .\n"
    )
  }

  test(
    "A single N-Triples statement with literal as object should have valid syntax"
  ) {
    val obj = Literal(objLiteral)
    val st = NtriplesStatement(subject, predicate, obj).toString
    assert(
      st == s"""<$subject> <$predicate> "$objLiteral" .\n"""
    )
  }

  test(
    "A single N-Triples statement with annotated literal as object should have valid syntax"
  ) {
    val obj = Literal(objLiteral, indicator)
    val st = NtriplesStatement(subject, predicate, obj).toString
    assert(
      st == s"""<$subject> <$predicate> "$objLiteral"@$indicator .\n"""
    )
  }

  test("N-Triples statements with a blank node should have valid syntax") {
    val objUri = URI(objUriLiteral)
    val objLiteral = Literal(objUriLiteral, indicator)
    val bn1 = BlankNode(predicate, objUri)
    val bn2 = BlankNode(predicate, objLiteral)
    val bns = BlankNodes(List(bn1, bn2))
    val st = NtriplesStatement(subject, predicate, bns)
    val hashCode = "xyz"
    val expectedRes =
      s"""_:$hashCode <$predicate> <$objUri> .\n
         |_:$hashCode <$predicate> "$objLiteral"@$indicator .\n
         |<$subject> <$predicate> _:$hashCode .\n""".stripMargin
    assert(
      st.obj
        .asInstanceOf[BlankNodes]
        .values
        .head
        .obj
        .asInstanceOf[URI]
        .value == objUriLiteral
    )
    assert(
      st.obj
        .asInstanceOf[BlankNodes]
        .values(1)
        .obj
        .asInstanceOf[Literal]
        .value == objUriLiteral
    )
  }
}
