/*
 * media-metadata-preprocessor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models.{ProcessingIgnore, ProcessingStatus, ReportingObject}
import org.apache.kafka.common.serialization.{
  StringDeserializer,
  StringSerializer
}
import org.apache.kafka.streams.{
  StreamsConfig,
  TestInputTopic,
  TestOutputTopic,
  Topology,
  TopologyTestDriver
}
import org.scalatest.funsuite.AnyFunSuite

import scala.language.reflectiveCalls
import java.io.File
import java.util.Properties
import scala.io.Source

case class TestSettings(
    applicationId: String,
    kafkaStreamsSettings: Properties,
    inputTopic: String,
    outputTopicAV: String,
    outputTopicImage: String,
    outputTopicIgnore: String,
    reportTopic: String,
    reportingStepName: String
) extends Settings

class KafkaTopologyTest extends AnyFunSuite {
  private val applicationId = "test"
  private val inputTopic = "test-in"
  private val outputTopicAV = "test-out-av"
  private val outputTopicIgnore = "test-out-none"
  private val outputTopicImage = "test-out-image"
  private val reportTopic = "test-report"
  private val props: Properties = {
    val p = new Properties()
    p.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId)
    p.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234")
    p
  }
  private val settings: Settings = TestSettings(
    applicationId,
    props,
    inputTopic,
    outputTopicAV,
    outputTopicImage,
    outputTopicIgnore,
    reportTopic,
    "test-step"
  )
  val testDriver: Topology => TopologyTestDriver = t =>
    new TopologyTestDriver(t, props)

  private def replaceTimestamp(value: String): String = {
    value.replaceAll("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d{3}", "2020")
  }

  private def readFile(name: String): String = {
    val s = Source.fromFile(new File(s"src/test/resources/topology/$name.nt"))
    val string = s.mkString
    s.close()
    string
  }

  private def createInputTopic(
      testDrive: TopologyTestDriver
  ): TestInputTopic[String, String] = {
    testDrive.createInputTopic(
      inputTopic,
      new StringSerializer(),
      new StringSerializer()
    )
  }

  private def createOutputTopicAV(
      testDrive: TopologyTestDriver
  ): TestOutputTopic[String, String] = {
    testDrive.createOutputTopic(
      outputTopicAV,
      new StringDeserializer(),
      new StringDeserializer()
    )
  }
  private def createOutputTopicImage(
      testDrive: TopologyTestDriver
  ): TestOutputTopic[String, String] = {
    testDrive.createOutputTopic(
      outputTopicImage,
      new StringDeserializer(),
      new StringDeserializer()
    )
  }
  private def createOutputTopicIgnored(
      testDrive: TopologyTestDriver
  ): TestOutputTopic[String, String] = {
    testDrive.createOutputTopic(
      outputTopicIgnore,
      new StringDeserializer(),
      new StringDeserializer()
    )
  }

  private def createReportsTopic(
      testDrive: TopologyTestDriver
  ): TestOutputTopic[String, String] = {
    testDrive.createOutputTopic(
      reportTopic,
      new StringDeserializer(),
      new StringDeserializer()
    )
  }

  private def createExpectedReport(
      key: String,
      processingStatus: ProcessingStatus,
      message: String
  ): String = {
    replaceTimestamp(
      ReportingObject(
        key,
        processingStatus,
        message,
        settings.reportingStepName
      ).toString
    )
  }
  test("ignore missing content") {
    val builder = new KafkaTopology
    val tD = testDriver(builder.build(settings))
    val inputTopic = createInputTopic(tD)
    inputTopic.pipeInput("", "")
    val reportingTopic = createReportsTopic(tD)
    val report = reportingTopic.readRecord()
    val expectedReport = createExpectedReport(
      "",
      ProcessingIgnore,
      "Record has no locator. Assuming there is no attached media file."
    )
    assertResult("")(report.key())
    assertResult(expectedReport)(replaceTimestamp(report.value()))
    tD.close()
  }

  test("srfaudio input") {
    val builder = new KafkaTopology
    val tD = testDriver(builder.build(settings))
    val inputTopic = createInputTopic(tD)
    inputTopic.pipeInput("test", readFile("input"))
    val outputTopicAV = createOutputTopicIgnored(tD)
    val record = outputTopicAV.readRecord()
    val reportingTopic = createReportsTopic(tD)
    val report = reportingTopic.readRecord()
    val expectedReport = createExpectedReport(
      "test",
      ProcessingIgnore,
      "Record is not enrichable; ebucore:isDistributedOn set to `srfaudio` for https://memobase.ch/digital/test-1"
    )
    assertResult("test")(report.key())
    assertResult(expectedReport)(replaceTimestamp(report.value()))

    assertResult("test")(record.key())
    assertResult(readFile("output"))(record.value())
    tD.close()

  }
}
