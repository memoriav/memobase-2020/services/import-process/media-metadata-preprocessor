/*
 * media-metadata-preprocessor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models.*
import ch.memobase.LocatorExtraction.*
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Named
import org.apache.kafka.streams.scala.ImplicitConversions.*
import org.apache.kafka.streams.scala.kstream.{Branched, KStream}
import org.apache.kafka.streams.scala.{StreamsBuilder, serialization}
import org.apache.logging.log4j.scala.Logging

class KafkaTopology extends Logging {

  import serialization.Serdes.*

  def build(settings: Settings): Topology = {
    val builder = new StreamsBuilder

    val source =
      builder.stream[String, String](settings.inputTopic)

    // val Array(recordWithLocator, locatorlessRecord) = checkForLocators(source)
    val maybeHasLocator = checkForLocators(source)
    // val Array(enrichableRecordAV, enrichableRecordImage, unenrichableRecord) =
    val maybeEnrichable = maybeHasLocator
      .get("locator-true")
      .map(checkIfEnrichable)
      .getOrElse(Map())

    maybeHasLocator
      .get("locator-false")
      .foreach(sendRecordsDownstream(_, settings.outputTopicIgnore))

    maybeEnrichable
      .get("media-none")
      .foreach(sendRecordsDownstream(_, settings.outputTopicIgnore))
    maybeEnrichable
      .get("media-av")
      .foreach(sendRecordsDownstream(_, settings.outputTopicAV))
    maybeEnrichable
      .get("media-image")
      .foreach(sendRecordsDownstream(_, settings.outputTopicImage))

    maybeEnrichable
      .get("media-av")
      .foreach(reportEnrichableRecords(_, settings.reportTopic, settings.reportingStepName))
    maybeEnrichable
      .get("media-image")
      .foreach(reportEnrichableRecords(_, settings.reportTopic, settings.reportingStepName))
    maybeHasLocator
      .get("locator-false")
      .foreach(reportLocatorlessRecords(_, settings.reportTopic, settings.reportingStepName))
    maybeEnrichable
      .get("media-none")
      .foreach(reportUnenrichableRecords(_, settings.reportTopic, settings.reportingStepName))

    builder.build()
  }

  private def checkIfEnrichable(
      stream: KStream[String, (String, List[ResourceWithLocator])]
  ) =
    stream
      .mapValues(v => setMediaPlayerType(v))
      .split(Named.as("media-"))
      .branch(
        (_, v) =>
          v._2.forall(_.enrichable) && v._2.exists(_.player != ImageViewer),
        Branched.as("av")
      ) // AV media with optional thumbnail
      .branch(
        (_, v) => v._2.exists(v => v.enrichable && v.player == ImageViewer),
        Branched.as("image")
      ) // Either image resource or thumbnail
      .defaultBranch(Branched.as("none"))

  private def checkForLocators(stream: KStream[String, String]) =
    stream
      .mapValues((k, v) => getEbucoreLocators(k, v))
      .split(Named.as("locator-"))
      .branch((_, v) => v._2.nonEmpty, Branched.as("true"))
      .defaultBranch(Branched.as("false"))

  private def setMediaPlayerType(
      value: (String, List[ResourceWithLocator])
  ): (String, List[ResourceWithLocator]) = {
    val newProperties = value._2.foldLeft("")((agg, x) =>
      agg + "\n" + NtriplesStatement(
        x.resource,
        "http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#isDistributedOn",
        Literal(x.player.name)
      ).toString
    )
    (value._1 + newProperties, value._2)
  }

  private def sendRecordsDownstream[T](
      records: KStream[String, (String, T)],
      outputTopic: String
  ): Unit =
    records
      .mapValues(_._1)
      .to(outputTopic)

  private def reportEnrichableRecords(
      enrichableRecord: KStream[String, (String, List[ResourceWithLocator])],
      reportTopic: String,
      reportingStepName: String
  ): Unit =
    enrichableRecord
      .mapValues((k, v) => {
        val distributedOn = v._2
          .map(x =>
            s"ebucore:isDistributedOn set to `${x.player.name}` for ${x.resource}"
          )
          .mkString("; ")
        ReportingObject(
          k,
          ProcessingSuccess,
          s"Record is enrichable; $distributedOn",
          reportingStepName
        ).toString
      })
      .to(reportTopic)

  private def reportUnenrichableRecords(
      unenrichableRecord: KStream[String, (String, List[ResourceWithLocator])],
      reportTopic: String,
      reportingStepName: String
  ): Unit =
    unenrichableRecord
      .mapValues((k, v) => {
        val distributedOn = v._2
          .map(x =>
            s"ebucore:isDistributedOn set to `${x.player.name}` for ${x.resource}"
          )
          .mkString("; ")
        ReportingObject(
          k,
          ProcessingIgnore,
          s"Record is not enrichable; $distributedOn",
          reportingStepName
        ).toString
      })
      .to(reportTopic)

  private def reportLocatorlessRecords(
      locatorlessRecord: KStream[String, (String, List[ResourceWithLocator])],
      reportTopic: String,
      reportingStepName: String
  ): Unit =
    locatorlessRecord
      .map((k, _) =>
        (
          k,
          ReportingObject(
            k,
            ProcessingIgnore,
            "Record has no locator. Assuming there is no attached media file.",
            reportingStepName
          ).toString
        )
      )
      .to(reportTopic)
}
