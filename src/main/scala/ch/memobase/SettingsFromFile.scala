/*
 * media-metadata-preprocessor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import java.util.Properties
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.scala.serialization.Serdes
import org.apache.kafka.common.config.SslConfigs

trait Settings {
  val applicationId: String
  val kafkaStreamsSettings: Properties
  val inputTopic: String
  val outputTopicAV: String
  val outputTopicImage: String
  val outputTopicIgnore: String
  val reportTopic: String
  val reportingStepName: String
}

object SettingsFromFile extends Settings {
  val applicationId: String = sys.env("APPLICATION_ID")

  val kafkaStreamsSettings: Properties = {
    val streamsProperties = new Properties()
    streamsProperties.setProperty(
      StreamsConfig.APPLICATION_ID_CONFIG,
      applicationId
    )
    streamsProperties.setProperty(
      StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,
      sys.env("KAFKA_BOOTSTRAP_SERVERS")
    )
    streamsProperties.setProperty(
      StreamsConfig.CLIENT_ID_CONFIG,
      applicationId
    )
    streamsProperties.setProperty(
      StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,
      Serdes.stringSerde.getClass.getName
    )
    streamsProperties.setProperty(
      StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,
      Serdes.stringSerde.getClass.getName
    )
    streamsProperties.setProperty(
      StreamsConfig.SECURITY_PROTOCOL_CONFIG,
      sys.env("KAFKA_SECURITY_PROTOCOL")
    )
    streamsProperties.setProperty(
      SslConfigs.SSL_KEYSTORE_TYPE_CONFIG,
      sys.env("KAFKA_SSL_KEYSTORE_TYPE")
    )
    streamsProperties.setProperty(
      SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG,
      sys.env("KAFKA_SSL_KEYSTORE_LOCATION")
    )
    streamsProperties.setProperty(
      SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG,
      sys.env("JKS_KEY_PASSWORD")
    )
    streamsProperties.setProperty(
      SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG,
      sys.env("KAFKA_SSL_TRUSTSTORE_LOCATION")
    )
    streamsProperties.setProperty(
      SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG,
      sys.env("KAFKA_SSL_TRUSTSTORE_TYPE")
    )
    streamsProperties
  }

  val inputTopic: String = sys.env("TOPIC_IN")

  val outputTopicAV: String = sys.env("TOPIC_OUT_AV")

  val outputTopicImage: String = sys.env("TOPIC_OUT_IMAGE")

  val outputTopicIgnore: String = sys.env("TOPIC_OUT_IGNORE")

  val reportTopic: String = sys.env("TOPIC_PROCESS")

  val reportingStepName: String = sys.env("REPORTING_STEP_NAME")
}
