/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.models

import java.util.UUID.randomUUID

sealed trait NtriplesObject

case class BlankNodes(values: List[BlankNode]) extends NtriplesObject

case class BlankNode(predicate: String, obj: NtriplesObject) {
  override def toString: String = obj match {
    case o: URI => s"<$predicate> $o .\n"
    case o: Literal => s"<$predicate> $o .\n"
    case o: BlankNodes =>
      val blankNodeId = s"_:$randomUUID"
      s"<$predicate> $blankNodeId .\n" + o.values.map(l => s"$blankNodeId ${l.toString}").mkString
  }
}

object BlankNode {
  def apply(predicate: String, obj: NtriplesObject): BlankNode = new BlankNode(predicate, obj)
  def apply(predicate: String, obj: BlankNode*): BlankNode = new BlankNode(predicate, BlankNodes(obj.toList))
}

case class Literal(value: String, indicator: Option[String], indicatorType: IndicatorType) extends NtriplesObject {
  override def toString: String = indicator match {
    case Some(i) =>
      indicatorType match {
        case LanguageIndicator => s""""$value"@$i"""
        case DatatypeIndicator => s""""$value"^^<$i>"""
      }
    case None => s""""$value""""
  }
}

object Literal {
  def apply(value: String): Literal = new Literal(value, None, LanguageIndicator)

  def apply(value: String, indicator: String): Literal = new Literal(value, Some(indicator), LanguageIndicator)

  def apply(value: String, indicator: String, indicatorType: IndicatorType): Literal = new Literal(value, Some(indicator), indicatorType)
}

case class URI(value: String) extends NtriplesObject {
  override def toString: String = s"<$value>"
}

case class NtriplesStatement(subject: String, predicate: String, obj: NtriplesObject) {
  override def toString: String = {
    obj match {
      case o: URI => s"<$subject> <$predicate> $o .\n"
      case o: Literal => s"<$subject> <$predicate> $o .\n"
      case o: BlankNodes =>
        val blankNodeId = s"_:$randomUUID"
        s"<$subject> <$predicate> $blankNodeId .\n" + o.values.map(l => s"$blankNodeId ${l.toString}").mkString
    }
  }
}

object NtriplesStatement {
  def apply(subject: String, predicate: String, obj: NtriplesObject): NtriplesStatement = new NtriplesStatement(subject, predicate, obj)
  def apply(subject: String, predicate: String, obj: BlankNode*): NtriplesStatement = new NtriplesStatement(subject, predicate, BlankNodes(obj.toList))
}

sealed trait IndicatorType
case object LanguageIndicator extends IndicatorType
case object DatatypeIndicator extends IndicatorType
