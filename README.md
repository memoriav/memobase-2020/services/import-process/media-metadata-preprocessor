# Media Metadata Preprocessor

This service “preprocesses” the data for media metadata enrichment in two ways:

- It defines the ebucore:isDistributedOn property. This property contains the information with what player the media information is delivered. It can have the following values:
  - `srfaudio`: Audio files via SRF Play
  - `srfvideo`: Video files via SRF Play
  - `vimeo`: Vimeo player
  - `youtube`: Youtube player
  - `zem`: ZEM player
  - `audio`: Local or remote audio files via Memobase player
  - `image`: Local or remote image files via Memobase player
  - `video`: Local or remote video files via Memobase player
- It decides on what information can at most be extracted from the referenced media file. The following variants are covered (in decreasing order of priority):
  - Referenced media file is accessible (i.e. can directly be downloaded for analysis) and is either video or audio: The data is sent to the import-process-av-enrichment topic.
  - The data references a poster image or the referenced media file is itself an accessible image file: The data is sent to the import-process-image-enrichment topic.
  - The data does not contain a thumbnail or other accessible media files: The data is directly sent to the import-process-normalization topic, therefore leaving out completely the media metadata enrichment.

The rationale behind the second step is two allow a more fine-grained resource control in our Kubernetes cluster as well as accelerate the import process. AV-processing / -analysing is resource and time intensive, so it does make sense to provide more resources to this step (which is unneeded for image analysis). On the other hand data which does not require this processing step can circumvent this potential bottleneck by using a “faster line” via the image enrichment service or even going directly to the normalization service.

## Configuration

In order to work as expected, the service needs to have a couple of environment variables set:

* `APPLICATION_ID`: Kafka Streams application identifier (see [Kafka documentation](https://kafka.apache.org/documentation/#streamsconfigs_application.id) for details)
* `KAFKA_BOOTSTRAP_SERVERS`: Comma-separated list of the Kafka cluster's bootstrap servers
* `TOPIC_IN`: Kafka topic where incoming topics are read from
* `TOPIC_OUT_AV`: Kafka topic where document messages describing an AV resource are written to
* `TOPIC_OUT_IMAGE`: Kafka topic where document messages describing an image resource are written to
* `TOPIC_OUT_IGNORE`: Kafka topic where document messages describing a resource which cannot be enriched are written to
* `TOPIC_PROCESS`: Kafka topic where status reports are written to
* `REPORTING_STEP_NAME`: Application name as shown in reports
* `KAFKA_SECURITY_PROTOCOL: Protocol used to communicate with brokers. Valid values are: `PLAINTEXT`, `SSL`, `SASL_PLAINTEXT`, `SASL_SSL`
* `KAFKA_SSL_KEYSTORE_TYPE: The file format of the key store file. This is optional for client. 
* `KAFKA_SSL_KEYSTORE_LOCATION: The location of the key store file. This is optional for client and can be used for two-way authentication for client
* `KAFKA_SSL_KEYSTORE_KEY`: Private key in the format specified by `KAFKA_SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with PKCS#8 keys. If the key is encrypted, key password must be specified using `JKS_KEY_PASSWORD`
* `KAFKA_SSL_KEYSTORE_CERTIFICATE_CHAIN`: Certificate chain in the format specified by `KAFKA_SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with a list of X.509 certificates
* `KAFKA_SSL_TRUSTSTORE_TYPE: The file format of the trust store file. The values currently supported by the default `ssl.engine.factory.class` are `JKS`, `PKCS12` and `PEM`
* `KAFKA_SSL_TRUSTSTORE_LOCATION: The location of the trust store file
* `KAFKA_SSL_TRUSTSTORE_CERTIFICATES`: Trusted certificates in the format specified by `KAFKA_SSL_TRUSTSTORE_TYPE`. Default SSL engine factory supports only PEM format with X.509 certificates
* `JKS_KEY_PASSWORD`: The password of the private key in the Kafka key store file or the PEM key specified in `KAFKA_SSL_KEYSTORE_KEY`.

Furthermore, the following file must be present when communicating with a protected Kafka cluster:

* Key store file in the format defined in `KAFKA_SSL_KEYSTORE_TYPE` and at the path defined in `KAFKA_SSL_KEYSTORE_LOCATION`
* Key store certificate chain in PEM format at the path defined in `KAFKA_SSL_KEYSTORE_CERTIFICATE_CHAIN`
* Key store key in PEM format at the path defined in `KAFKA_SSL_KEYSTORE_KEY`
* Trust store file in the format defined in `KAFKA_SSL_TRUSTSTORE_TYPE` and at the path defined in `KAFKA_SSL_TRUSTSTORE_LOCATION`
* Trust store certificates in PEM format at the path defined in `KAFKA_SSL_TRUSTSTORE_CERTIFICATES`
